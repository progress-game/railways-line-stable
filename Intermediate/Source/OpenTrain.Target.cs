using UnrealBuildTool;

public class OpenTrainTarget : TargetRules
{
	public OpenTrainTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Game;
		ExtraModuleNames.Add("OpenTrain");
	}
}
